﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DapperPostgres;

internal class Product
{
    public int Id { get; set; }
    public string Name { get; set; }
    public string Description { get; set; }
    public int StockQuantity { get; set; }
    public decimal Price { get; set; }

    public Product(int id, string name, string description, int stockQuantity, decimal price)
    {
        Id = id;
        Name = name;
        Description = description;
        StockQuantity = stockQuantity;
        Price = price;
    }

    public override string ToString()
    {
        if (Description.Length > 10)
        {
            return $"({Id}) {Name}, Description: {Description[..10]}..., Stock quantity: {StockQuantity}, Price: {Price:C}";
        }
        else
        {
            return $"({Id}) {Name}, Description: {Description}, Stock quantity: {StockQuantity}, Price: {Price:C}";
        }
    }
}
