﻿using Dapper;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DapperPostgres;

internal class DataBase
{
    private readonly string _connString;

    public DataBase(string connectionString) => _connString = connectionString;

    public IEnumerable<Customer> GetCustomers(int? count = null)
    {
        using NpgsqlConnection conn = new(_connString);
        string sql;
        if (count.HasValue)
        {
            if (count.Value < 1) 
            { 
                throw new ArgumentException("Число строк должно быть положительным ненулевым числом.", nameof(count));
            }
            sql = "SELECT id, firstname, lastname, age FROM customers LIMIT @count";
            return conn.Query<Customer>(sql, new { count = count.Value });
        }
        sql = "SELECT id, firstname, lastname, age FROM customers";
        return conn.Query<Customer>(sql);
    }

    public IEnumerable<Order> GetOrders(int? count = null)
    {
        using NpgsqlConnection conn = new(_connString);
        string sql;
        if (count.HasValue)
        {
            if (count.Value < 1)
            {
                throw new ArgumentException("Число строк должно быть положительным ненулевым числом.", nameof(count));
            }
            sql = "SELECT id, CustomerId, ProductId, Quantity FROM orders LIMIT @count";
            return conn.Query<Order>(sql, new { count = count.Value });
        }
        else
        {
            // Артур, у меня появились сомнения на счет того, что else здесь не нужен.
            // Технически он не нужен и добавляет 3 строки в код. Но с другой стороны
            // он улучшает читаемость, снимая с программиста когнитивную нагрузку.
            // Читающему код не нужно следить кончился ли блок if командой return и логически
            // сразу видно что этот блок выполняется не просто так, а когда условие ложно.
            // И если читающий просто хочет увидеть "а что будет если условие ложно" он
            // сразу безошибочно придет сюда и ему не придется просматривать блок if.
            sql = "SELECT id, CustomerId, ProductId, Quantity FROM orders";
            return conn.Query<Order>(sql);
        }
    }

    public IEnumerable<Product> GetProducts(int? count = null)
    {
        using NpgsqlConnection conn = new(_connString);
        string sql;
        if (count.HasValue)
        {
            if (count.Value < 1)
            {
                throw new ArgumentException("Число строк должно быть положительным ненулевым числом.", nameof(count));
            }
            sql = "SELECT * FROM products LIMIT @count";
            return conn.Query<Product>(sql, new { count = count.Value });
        }
        sql = "SELECT * FROM products";
        return conn.Query<Product>(sql);
    }

    public IEnumerable<Customer> GetCustomersOlderThan(int ageFrom)
    {
        using NpgsqlConnection conn = new(_connString);
        string sql = "SELECT * FROM customers WHERE age > @ageFrom";
        return conn.Query<Customer>(sql, new { ageFrom });
    }

    public bool AddCustomer(Customer customer)
    {
        using NpgsqlConnection conn = new(_connString);
        string sql = "INSERT INTO Customers(FirstName, LastName, Age) " + 
                     "VALUES (@FirstName, @LastName, @Age)";
        int affectedRows = conn.Execute(sql, 
            new { customer.FirstName, customer.LastName, customer.Age });
        if (affectedRows > 0)
        {
            return true;
        }
        return false;
    }

    public Customer? GetFirstOrDefaultCustomer(int id)
    {
        using NpgsqlConnection conn = new(_connString);
        string sql = "SELECT * FROM Customers WHERE Id = @id";
        return conn.QuerySingleOrDefault<Customer>(sql, new { id });
    }

    public Customer? GetFirstOrDefaultCustomer(string firstName, 
        string lastName, 
        int? age = null)
    {
        using NpgsqlConnection conn = new(_connString);
        string sql;
        if (age.HasValue)
        {
            sql = "SELECT * FROM Customers " +
                "WHERE firstname = @firstName AND lastname = @lastName AND age = @age";
            return conn.QueryFirstOrDefault<Customer>(sql, 
                new { firstName, lastName, age = age.Value });
        }
        sql = "SELECT * FROM Customers " +
            "WHERE firstname = @firstName AND lastname = @lastName";
        return conn.QueryFirstOrDefault<Customer>(sql, new { firstName, lastName });
    }

    public bool RemoveCustomer(Customer customer) => RemoveCustomer(customer.Id);

    public bool RemoveCustomer(int id)
    {
        using NpgsqlConnection conn = new(_connString);
        string sql = "DELETE FROM Customers WHERE Id = @id";
        int affectedRows = conn.Execute(sql, new { id });
        if (affectedRows > 0)
        {
            return true;
        }
        return false;
    }

    public Product? GetProduct(int id)
    {
        using NpgsqlConnection conn = new(_connString);
        string sql = "SELECT * FROM Products WHERE Id = @id";
        return conn.QuerySingleOrDefault<Product>(sql, new { id });
    }

    public bool UpdateProductStockQuantity(int id, int newStockQuantity)
    {
        using NpgsqlConnection conn = new(_connString);
        string sql = "UPDATE Products SET stockquantity = @newStockQuantity " +
                    "WHERE id = @id";
        int affectedRows = conn.Execute(sql, new { newStockQuantity, id });
        if (affectedRows > 0)
        {
            return true;
        }
        return false;
    }

    private class HomeworkReturn
    {
        public int CustomerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int ProductId { get; set; }
        public int ProductQuantity { get; set; }
        public decimal ProductPrice { get; set; }

        public HomeworkReturn(int customerId, string firstName, string lastName, 
            int productId, int productQuantity, decimal productPrice)
        {
            CustomerId = customerId;
            FirstName = firstName;
            LastName = lastName;
            ProductId = productId;
            ProductQuantity = productQuantity;
            ProductPrice = productPrice;
        }

        public override string ToString()
        {
            return $"CustomerId = {CustomerId}, FirstName = {FirstName}, LastName = {LastName},\n" +
                $"\tProductId = {ProductId}, ProductQuantity = {ProductQuantity}, ProductPrice = {ProductPrice:C}";
        }
    }

    public void ExecuteQueryFromLastHomework(int age, int id)
    {
        /*Написать запрос, который возвращает список всех пользователей старше 30 лет, 
         * у которых есть заказ на продукт с ID=2. Используйте alias, чтобы дать столбцам в 
         * результирующей выборке понятные названия. В результате должны получить таблицу:
         * CustomerID, FirstName, LastName, ProductID, ProductQuantity, ProductPrice*/
        using NpgsqlConnection conn = new(_connString);
        string sql = "SELECT CustomerID, FirstName, LastName, ProductID, " +
            "   Quantity as ProductQuantity, Price as ProductPrice " +
            "FROM Customers as c " +
            "JOIN Orders as o ON c.ID = o.CustomerID " +
            "JOIN Products as p ON o.ProductID = p.ID " +
            "WHERE c.Age > @age AND p.ID = @id";
        HomeworkReturn? result = conn.QueryFirstOrDefault<HomeworkReturn>(sql, new { age, id });
        Console.WriteLine(result is not null ? result : "query returned nothing");
    }
}
