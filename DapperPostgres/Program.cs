﻿/* Домашнее задание

Dapper / Linq2sql
 *   Определиться с ORM: Dapper (Linq2Db).
 *   Выбрать какую БД использовать (из задания "Sql запросы" или "Кластерный индекс"), написать строку подключения к БД и 
        использовать ее для подключения. (опираться можно на пример из материалов)
 *   Создать классы, которые описывают таблицы в БД
 *   Используя ORM выполнить простые запросы к каждой таблице, выполнить параметризованные запросы к каждой таблице (без JOIN) - 
        2-3 запроса на таблицу.
 *   Значения параметров для фильтрации можно как задавать из консоли, так и значениями переменных в коде. (пример GetStudent)
 *   Выполнить все запросы, из выбранного ранее задания с передачей параметров.
*/

namespace DapperPostgres;

internal class Program
{
    static void Main()
    {
        DataBase db = new(@"User ID=postgres;Password=postgres;Host=localhost;Port=5432;Database=Shop;");

        IEnumerable<Customer> customers = db.GetCustomers();
        PrintCollection(header: "*** Get all customers ***", customers);
        IEnumerable<Product> products = db.GetProducts(5);
        PrintCollection("*** Get 5 products ***", products);
        IEnumerable<Order> orders = db.GetOrders(3);
        PrintCollection("*** Get 3 orders ***", orders);
        IEnumerable<Customer> oldGuys = db.GetCustomersOlderThan(50);
        PrintCollection("*** Get all customers over 50 y.o. ***", oldGuys);
        Console.WriteLine("*** Add customer Howard Lovecraft ***");
        bool result = db.AddCustomer(new Customer(-1, "Howard", "Lovecraft", 72));
        Console.WriteLine(result ? "added" : "hasn't added");
        Console.WriteLine("*** Retrieve Howard ***");
        Customer? howard = db.GetFirstOrDefaultCustomer("Howard", "Lovecraft");
        Console.WriteLine(howard is not null ? howard.ToString() : "NULL");
        Console.WriteLine("*** Remove Howard ***");
        // такая запись легче читается, чем то, что VS предлагает в предупреждении
        result = howard is not null ? db.RemoveCustomer(howard) : false;
        Console.WriteLine(result ? "Howard removed" : "Howard hasn't removed");
        Console.WriteLine();
        Console.WriteLine("*** Get product with ID=3 ***");
        Product? product = db.GetProduct(3);
        Console.WriteLine(product is not null ? product.ToString() : "product not found");
        Console.WriteLine("*** Increment stock quantity ***");
        result = product is not null
            ? db.UpdateProductStockQuantity(product.Id, product.StockQuantity + 1)
            : false;
        Console.WriteLine(result ? "changed" : "hasn't changed");
        Console.WriteLine("*** Show updated product ***");
        product = db.GetProduct(3);
        Console.WriteLine(product is not null ? product.ToString() : "product not found");
        Console.WriteLine();
        Console.WriteLine("*** Execute query from last homework ***");
        db.ExecuteQueryFromLastHomework(age: 30, id: 2);
    }

    private static void PrintCollection<T>(string header, IEnumerable<T> collection)
    {
        Console.WriteLine(header);
        foreach (T item in collection)
        {
            Console.WriteLine(item);
        }
        Console.WriteLine();
    }

    // Содержимое консоли после запуска:
    /*
    *** Get all customers ***
    (1) Tom Sawyer - 13 y.o.
    (2) Gary Oldman - 56 y.o.
    (3) Peter Parker - 23 y.o.
    (4) Suzy Quatro - 42 y.o.
    (5) Albert Einstein - 64 y.o.
    (6) Vladimir Pupkin - 27 y.o.
    (7) Alla Pugacheva - 97 y.o.
    (8) Bill Gates - 30 y.o.
    (9) Kate Bush - 36 y.o.
    (10) Joan of Arc - 19 y.o.
    
    *** Get 5 products ***
    (1) nVidia 4090 Super, Description: Your shove..., Stock quantity: 50, Price: 84 999,00 ?
    (2) Hairbrush, Description: With 5D vo..., Stock quantity: 170, Price: 249,99 ?
    (4) Monopoly board game, Description: wont teach..., Stock quantity: 20, Price: 2 990,00 ?
    (5) Circa skateboard, Description: Take care ..., Stock quantity: 10, Price: 7 999,99 ?
    (6) Microsoft Windows Vista, Description: Are you pe..., Stock quantity: 4, Price: 1 990,00 ?
    
    *** Get 3 orders ***
    (1) Customer: 1, Product: 3, Quantity: 1
    (2) Customer: 2, Product: 6, Quantity: 2
    (2) Customer: 2, Product: 7, Quantity: 1
    
    *** Get all customers over 50 y.o. ***
    (2) Gary Oldman - 56 y.o.
    (5) Albert Einstein - 64 y.o.
    (7) Alla Pugacheva - 97 y.o.
    
    *** Add customer Howard Lovecraft ***
    added
    *** Retrieve Howard ***
    (22) Howard Lovecraft - 72 y.o.
    *** Remove Howard ***
    Howard removed
    
    *** Get product with ID=3 ***
    (3) Pikachu action figure, Description: pika pika, Stock quantity: 9, Price: 1 990,00 ?
    *** Increment stock quantity ***
    changed
    *** Show updated product ***
    (3) Pikachu action figure, Description: pika pika, Stock quantity: 10, Price: 1 990,00 ?
    
    *** Execute query from last homework ***
    CustomerId = 9, FirstName = Kate, LastName = Bush,
        ProductId = 2, ProductQuantity = 2, ProductPrice = 249,99 ?
    
    D:\Projects\DapperPostgres\DapperPostgres\bin\Debug\net7.0\DapperPostgres.exe (process 14920) exited with code 0.
    To automatically close the console when debugging stops, enable Tools->Options->Debugging->Automatically close the console when debugging stops.
    Press any key to close this window . . .
    */
}