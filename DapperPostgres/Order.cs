﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DapperPostgres;

internal class Order
{
    public int Id { get; set; }
    public int CustomerId { get; set; }
    public int ProductId { get; set; }
    public int Quantity { get; set; }

    public Order(int id, int customerId, int productId, int quantity)
    {
        Id = id;
        CustomerId = customerId;
        ProductId = productId;
        Quantity = quantity;
    }

    public override string ToString()
    {
        return $"({Id}) Customer: {CustomerId}, Product: {ProductId}, Quantity: {Quantity}";
    }
}
